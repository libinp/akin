import React, { useEffect, useState } from 'react';
import './App.scss';

function App() {

  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filter, setFilter] = useState([]);
  const [filterValue, setFilterValue] = useState('');

  useEffect(() => {
    fetch('https://rainy-star.surge.sh/products.json')
      .then(response => response.json())
      .then(response => {
        setProducts((prev) => ([
          ...response
        ]))
        setLoading(false);
        let filter = new Set();
        response.forEach(item => {
          item.size.sort(con => filter.add(con))
        })
        setFilter((prev) => [
          ...Array.from(filter)
        ]);
      })
  }, [])

  return (
    <section className="product__section">
      <div className='product__container'>
        <header className='product__header'>
          <div className='header__title'>
            <h1>Women's tops</h1>
          </div>
          <div className='product__filter'>
            <select className='form__control' defaultValue={'S'} onChange={(e) => setFilterValue((prev) => e.target.value)}>
              <option value=''>Filter by Size</option>
              {filter.map((item, index) => <option value={item} key={index}>{item}</option>)}
            </select>
          </div>
        </header>
        <div className='product__list'>
          {products.map((item, index) => 
            <React.Fragment key={index}>
              {(item.size.includes(filterValue) || filterValue === '') &&
                <div className='product__items' >
                  <img src={'/products/' + item.productImage} />
                  <div className='tags__list'>
                    {item.isSale && <span className='sale'>Sale</span>}
                    {item.isExclusive && <span>Exclusive</span>}
                  </div>
                  <div className='product__info'>
                    <h2>{item.productName}</h2>
                    <span className='product__price'>{item.price}</span>
                  </div>
                </div>}
            </React.Fragment>
          )}
        </div>
        {loading &&
          <div className='product__list'>
            {Array.from(Array(8), (e, i) => <div className='product__items' key={i}><Loading></Loading></div>)}
          </div>
        }
      </div>
    </section>
  );
}

function Loading() {
  return (
    <div className='loading-card'>
      <div className="loading_card_img "></div>
    </div>
  )
}

export default App;
