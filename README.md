# Getting Started

In the project directory, you can run:

`npm i` or `npm install`

## Running the App

In the project directory, you can run:

### `npm start` or `npm run dev` ( webpack config )

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) ( for npm start ) or [http://localhost:8080](http://localhost:8080) ( for npm run dev ) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Building the App

### `npm run build` ( non webpack ) `npm run prod` ( webpack config )

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

### For styling purpose used node-sass module

## Note 

react-router not used because of routing not used in this app. Redux also not used because of no state management require for this app. 
